# Pulumi counter

Goal: count how many times someone clicks a thing.

When the page loads, see how many times it's been clicked.

When you click, increase the count and display the new count.

Using Pulumi and Typescript.

This code basically comes from [the Pulumi examples](https://github.com/pulumi/examples/tree/master/aws-ts-apigateway).