import * as aws from '@pulumi/aws'
import * as awsx from '@pulumi/awsx'
import { APIGatewayProxyEvent } from 'aws-lambda'

const counterTable = new aws.dynamodb.Table('counterTable', {
    attributes: [{
        name: 'id',
        type: 'S'
    }],
    hashKey: 'id',
    readCapacity: 5,
    writeCapacity: 5
})

const endpoint = new awsx.apigateway.API('counter', {
  routes: [
    {
      path: '/{route+}',
      method: 'GET',
      eventHandler
    },
    {
      path: '/{route+}',
      method: 'POST',
      eventHandler
    }
  ]
})

async function eventHandler(event: APIGatewayProxyEvent) {
  const route = event.pathParameters!['route']

  const client = new aws.sdk.DynamoDB.DocumentClient()

  const tableData = await client.get({
      TableName: counterTable.name.get(),
      Key: { id: route },
      ConsistentRead: true
  }).promise()

  const value = tableData.Item
  let count = (value && value.count) || 0

  if (event.httpMethod === "POST") {
    await client.put({
      TableName: counterTable.name.get(),
      Item: { id: route, count: ++count }
    }).promise()
  }

  return {
      statusCode: 200,
      body: JSON.stringify({ route, count }),
      headers: { 'Access-Control-Allow-Origin': 'https://juuulia.nz' }
  }
}

exports.endpoint = endpoint.url